use clap::Parser;
use gio::prelude::FileExt;
use gio::File;
use log::{debug, info};

extern crate pretty_env_logger;

/// Set the cover art as icon for a given directory
#[derive(Parser)]
struct Cli {
    /// The directory to set the cover for
    path: std::path::PathBuf,
}

fn main() -> std::io::Result<()> {
    let args = Cli::parse();
    let dir = args.path;

    pretty_env_logger::init();

    if let Ok(info) =
        File::for_path(&dir).query_info("*", gio::FileQueryInfoFlags::NONE, gio::Cancellable::NONE)
    {
        for attr in info.list_attributes(None) {
            debug!("{:?}", attr);
        }
    }

    let icon: Option<std::path::PathBuf> = File::for_path(&dir)
        .query_info("*", gio::FileQueryInfoFlags::NONE, gio::Cancellable::NONE)
        .unwrap_or_default()
        .attribute_string("metadata::custom-icon")
        .map_or(None, |uri| gio::File::for_uri(&uri).path());

    let mut cover: Option<std::fs::DirEntry> = None;
    for entry in std::fs::read_dir(dir.clone())? {
        let entry = entry?;
        let name = entry.file_name();
        if name.eq_ignore_ascii_case("cover.jpg") {
            cover = Some(entry);
            break;
        }
    }
    if let Some(cover) = cover {
        info!("Found {:?} in {:?}", cover.file_name(), dir);
        if let Some(icon) = icon {
            if icon == cover.path() {
                info!("Icon and file match.");
            } else {
                info!("Icon and file don’t match.");
            }
        }
    }
    Ok(())
}
